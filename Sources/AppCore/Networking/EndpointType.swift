import Foundation
import Alamofire

public protocol EndpointType {
    var path: String { get }
    var baseUrl: String { get }
    var endpointUrl: String { get }
    var parameters: [String: Any]? { get }
    var encodableParameters: Data? { get }
    var method: Alamofire.HTTPMethod { get }
    var additionalHeaders: HTTPHeaders? { get }
    var encoding: ParameterEncoding { get }
    var shouldPerformEncoding: Bool { get }
    var formData: [FormDataType] { get }
    var storeWhenOffline: Bool { get }
    var offlineErrorStyle: OfflineErrorStyle { get }
}

public protocol FormDataType {
    var name: String? { get set }
    var value: Data? { get set }
    var fileName: String? { get set }
    var attachmentTypeValue: String? { get set }
    var isValid: Bool { get }
}

public enum OfflineErrorStyle {
    /// Discard error message completely
    case discard
    /// Present warning with request storage and provides callback
    case warning
    /// Present simple error
    case error
}
