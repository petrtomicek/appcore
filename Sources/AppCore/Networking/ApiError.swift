import Foundation

public enum ApiError: Error {
    
    case unknownError(status: Int)
    case unavailable(status: Int, message: String)
    case apiError(messageValue: String, status: Int, code: Int = 0)
    case parsing(message: String?)
    case offline(endpoint: EndpointType)
    case notAvailable
    case noIdentityToken
    case refreshTokenFailure
    case jwtExpired
    case userNotFound
    
    public init(messageValue: String, status: Int, errorCode: Int) {
        self = .apiError(messageValue: messageValue, status: status, code: errorCode)
    }
    
    public var title: String {
        switch self {
        case .offline: return "Offline" //Strings.apiErrorOfflineTitle
        default: return "Error" //Strings.generalErrorTitle
        }
    }
    
    public var subtitle: String {
        switch self {
        case .apiError(let messageValue, _, _): return messageValue
        case .offline: return "Offline" //Strings.apiErrorOfflineMessage
        default: return "Error subtitle" //Strings.generalErrorTitle
        }
    }
    
    public var status: Int {
        switch self {
        case .apiError(_, let status, _): return status
        default: return 400
        }
    }
    
    public var code: Int {
        switch self {
        case .apiError(_, _, let code): return code
        default: return 1
        }
    }
}
