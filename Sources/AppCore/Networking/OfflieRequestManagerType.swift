import Foundation

public protocol OfflineRequestManagerType: ManagerType {
    func startObservingNetwork()
    func persist(endpoint: EndpointType)
}
