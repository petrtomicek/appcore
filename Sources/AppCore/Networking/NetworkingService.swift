import Foundation
import Alamofire

public let kPropBackendUrl = "BASE_API_URL"

public protocol RequestInterceptorType: RequestInterceptor {
    var apiService: NetworkingServiceType? { get set }
}

public protocol FileUploadProgressDelegate: AnyObject {
    func progress(value: Double) -> Void
    func associate(request: UploadRequest)
}

public protocol NetworkingServiceType: ServiceType {
    func requestAsync<T: Decodable>(endpoint: EndpointType) async throws -> T
    func request<T: Decodable>(endpoint: EndpointType, completion: @escaping (Swift.Result<T, ApiError>) -> Void)
    func requestArray<T: Decodable>(endpoint: EndpointType, returnType: T.Type) async throws -> T
    func uploadWithProgress<T: Decodable>(endpoint: EndpointType, returnType: T.Type, progressDelegate: FileUploadProgressDelegate?) async throws -> T
    func downloadFile(endpoint: EndpointType) async throws -> URL?
}

open class NetworkingService {

    @Dependency(property: kPropBackendUrl) private var baseUrl: String
    @Dependency<OfflineRequestManagerType> private var offlineManager

    var jsonDecoder: JSONDecoder = JSONDecoder()
    public var interceptor: RequestInterceptorType?
    private lazy var sessionManager: Session = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 120
        configuration.timeoutIntervalForResource = 600
        configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        interceptor?.apiService = self
        return Session(configuration: configuration, interceptor: interceptor)
    }()

    public init() { }

    private func createPath(for endpoint: EndpointType) -> URLConvertible {
        let url = baseUrl + endpoint.path
        return try! url.asURL()
    }
}

extension NetworkingService: NetworkingServiceType {
    
    public func request<T: Decodable>(endpoint: EndpointType, completion: @escaping (Swift.Result<T, ApiError>) -> Void) {
        let url = createPath(for: endpoint)
        let request = sessionManager.request(url, method: endpoint.method, parameters: endpoint.parameters, encoding: endpoint.encoding, headers: endpoint.additionalHeaders)
        request.validate().responseData { [weak self] response in
            guard let self else { return }
            do {
                let result = try self.process(data: response, endpoint: endpoint, returnType: T.self)
                completion(.success(result))
            } catch {
                completion(.failure(error as! ApiError))
            }
        }
    }
    
    public func requestAsync<T: Decodable>(endpoint: EndpointType) async throws -> T {
        let url = createPath(for: endpoint)
        let request = sessionManager.request(url, method: endpoint.method, parameters: endpoint.parameters, encoding: endpoint.encoding, headers: endpoint.additionalHeaders)
        let data = await request.validate().serializingData().response
        do {
            let result = try self.process(data: data, endpoint: endpoint, returnType: T.self)
            return result
        } catch {
            throw error as! ApiError
        }
    }
    
    public func requestArray<T: Decodable>(endpoint: EndpointType, returnType: T.Type) async throws -> T {
        let url = createPath(for: endpoint)
        let request = sessionManager.request(url, method: endpoint.method, parameters: endpoint.encodableParameters, encoder: ArrayParametersEncoding(), headers: endpoint.additionalHeaders)
        let data = await request.validate().serializingData().response
        do {
            let result = try self.process(data: data, endpoint: endpoint, returnType: T.self)
            return result
        } catch {
            throw error as! ApiError
        }
    }
    
    public func process<T: Decodable>(data: DataResponse<Data, AFError>, endpoint: EndpointType, returnType: T.Type) throws -> T {
        do {
            let httpStatusCode = data.response?.statusCode ?? 500
            switch data.result {
            case .success(let responseData):
                if httpStatusCode == 204 {
                    return VoidResponse() as! T
                }
                let decoder = JSONDecoder()
                let object = try decoder.decode(T.self, from: responseData)
                return object
            case .failure(let afError):
                if evaluateOfflineState(error: afError as NSError, endpoint: endpoint) {
                    throw ApiError.offline(endpoint: endpoint)
                }
                if httpStatusCode > 499 {
                    throw ApiError.unavailable(status: httpStatusCode, message: afError.localizedDescription)
                }
                if httpStatusCode == 403 {
                    throw ApiError.jwtExpired
                }
                if let responseData = data.data {
                    let decoder = JSONDecoder()
                    let errorResponse = try decoder.decode(ErrorResponse.self, from: responseData)
                    throw ApiError(messageValue: errorResponse.message ?? "unknown", status: httpStatusCode, errorCode: errorResponse.code)
                }
                throw ApiError.unknownError(status: httpStatusCode)
            }
        } catch {
            print("API error: \(error.localizedDescription)")
            throw ApiError.apiError(messageValue: error.localizedDescription, status: 0, code: 0)
        }
    }
    
    public func uploadWithProgress<T: Decodable>(endpoint: EndpointType, returnType: T.Type, progressDelegate: FileUploadProgressDelegate?) async throws -> T {
        let url = createPath(for: endpoint)
        let request = sessionManager.upload(
            multipartFormData: { (data) in
                for formData in endpoint.formData.filter({ $0.isValid }) {
                    guard let name = formData.name else { continue }
                    if let fileName = formData.fileName, let fileData = formData.value, let mimeType = formData.attachmentTypeValue {
                        data.append(fileData, withName: name, fileName: fileName, mimeType: mimeType)
                    } else {
                        data.append(formData.value ?? Data(), withName: name)
                    }
                }
            },
            to: url,
            method: endpoint.method,
            headers: endpoint.additionalHeaders
        )
        progressDelegate?.associate(request: request)
        let data = await request.uploadProgress(closure: { uploadProgress in
            progressDelegate?.progress(value: uploadProgress.fractionCompleted)
            print("Upload Progress: \(uploadProgress.fractionCompleted)")
        }).validate().serializingData().response
        do {
            let result = try self.process(data: data, endpoint: endpoint, returnType: T.self)
            return result
        } catch {
            throw error as! ApiError
        }
    }
    
    public func downloadFile(endpoint: EndpointType) async throws -> URL? {
        do {
            let destination: DownloadRequest.Destination = { temporaryURL, response in
                let temporaryDirectory = NSTemporaryDirectory()
                let filePath = "\(temporaryDirectory)\(response.suggestedFilename ?? "data.zip")"
                let url = URL(fileURLWithPath: filePath)
                return (url, [.createIntermediateDirectories, .removePreviousFile])
            }
            let url = createPath(for: endpoint)
            let request = sessionManager
                .download(url, method: endpoint.method, parameters: endpoint.parameters, encoding: endpoint.encoding, headers: endpoint.additionalHeaders, to: destination)
                .downloadProgress(closure: { progress in
                    print("Dowload progress: \(progress)")
                })
            let data = await request.validate().serializingDownloadedFileURL().response
            switch data.result {
            case .success(let url):
                return url
            case .failure(let error):
                throw ApiError.apiError(messageValue: error.localizedDescription, status: 0)
            }
        }
    }
    
    private func evaluateOfflineState(error: NSError, endpoint: EndpointType) -> Bool {
        let code = error.code
        if code == -1009 || code == -1020 || error.description.contains(insensitive: "-1009") ||
            error.localizedDescription.contains("offline") ||
            error.localizedDescription.contains("504") ||
            error.localizedDescription.contains("502") {
            offlineManager.persist(endpoint: endpoint)
            return true
        }
        return false
    }
}
