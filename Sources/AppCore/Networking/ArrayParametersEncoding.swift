import Foundation
import Alamofire

private let arrayParametersKey = "arrayParametersKey"

/// Extenstion that allows an array be sent as a request parameters
public extension Array {
    /// Convert the receiver array to a `Parameters` object.
    func asParameters() -> Data {
        guard let data = self as? [[String: Any]] else { return Data() }
        return try! JSONSerialization.data(withJSONObject: [arrayParametersKey: data], options: [])
    }
    
    func asIntParameters() -> Data {
        guard let _ = self as? [Int] else { return Data() }
        return try! JSONSerialization.data(withJSONObject: [arrayParametersKey: self], options: [])
    }
    
    func asStringParameters() -> Data {
        guard let _ = self as? [String] else { return Data() }
        return try! JSONSerialization.data(withJSONObject: [arrayParametersKey: self], options: [])
    }
}

/// Convert the parameters into a json array, and it is added as the request body.
/// The array must be sent as parameters using its `asParameters` method.
public class ArrayParametersEncoding: ParameterEncoder {
    
    public let encoder: JSONEncoder
    
    /// Creates an instance with the provided `JSONEncoder`.
    ///
    /// - Parameter encoder: The `JSONEncoder`. `JSONEncoder()` by default.
    public init(encoder: JSONEncoder = JSONEncoder()) {
        self.encoder = encoder
    }
    
    public func encode<Parameters: Encodable>(_ parameters: Parameters?, into request: URLRequest) throws -> URLRequest {
        guard let parameters = parameters else { return request }
        let decoded = try JSONSerialization.jsonObject(with: parameters as! Data)
        guard let dictionary = decoded as? [String:Any], let array = dictionary[arrayParametersKey] else { return request }
        var request = request
        do {
            let data = try JSONSerialization.data(withJSONObject: array, options: [])
            request.httpBody = data
            if request.headers["Content-Type"] == nil {
                request.headers.update(.contentType("application/json"))
            }
        } catch {
            throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
        }
        return request
    }
}
