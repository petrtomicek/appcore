import Foundation
import AuthenticationServices
import KeychainSwift

public class AppleIdLoginManager: NSObject, OAuthProviderType {
    
    private let _firstNameKey = "appleId.firstname"
    private let _lastNameKey = "appleId.lastname"
    
    private let permissionScopes: [ASAuthorization.Scope]
    private var completion: ((Result<String, OAuthAuthenticationError>) -> Void)?
    private var view: UIViewController!
    private let keychain = KeychainSwift()

    public var providerType: AuthenticationType { return .apple }

    public init(permissionScopes: [ASAuthorization.Scope]) {
        self.permissionScopes = permissionScopes
    }
    
    public func authenticate(from viewController: UIViewController, completion: @escaping (Result<String, OAuthAuthenticationError>) -> Void) {
        self.view = viewController
        self.completion = completion
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.performRequests()
    }
}

extension AppleIdLoginManager: ASAuthorizationControllerDelegate {
    
    public func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let auth = authorization.credential as? ASAuthorizationAppleIDCredential {
            if let fullName = auth.fullName, let lastname = fullName.familyName, let firstName = fullName.givenName {
                keychain.set(firstName, forKey: _firstNameKey)
                keychain.set(lastname, forKey: _lastNameKey)
            }
            guard
                let token = auth.identityToken,
                let tokenString = String(data: token, encoding: .utf8),
                let code = String(data: auth.authorizationCode!, encoding: .utf8)
            else { return }
            let appleIdResponse = AppleIdResponse(
                firstName: keychain.get(_firstNameKey),
                lastName: keychain.get(_lastNameKey),
                token: tokenString,
                code: code
            )
            do {
                let data = try JSONEncoder().encode(appleIdResponse)
                completion?(.success(String(decoding: data, as: UTF8.self)))
            } catch {
                completion?(.failure(.unknown(status: error.localizedDescription, code: (error as NSError?)?.code)))
            }
        }
    }

    public func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        guard [1001, 1000].notContains((error as NSError).code) else {
            completion?(.failure(.cancelledByUser))
            return
        }
        completion?(.failure(.unknown(status: error.localizedDescription, code: (error as NSError?)?.code)))
    }
}

public struct AppleIdResponse: Codable {
    
    public let firstName: String?
    public let lastName: String?
    public let token: String
    public let code: String
}
