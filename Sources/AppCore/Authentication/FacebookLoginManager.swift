import Foundation
import AuthenticationServices

public enum FbPermissionScopes: String {
    case publicProfile = "public_profile"
    case email = "email"
}

public enum FbTokenType: String {
    case code = "code"
    case token = "token"
    
    public var tokenName: String {
        switch self {
            case .code: return "code"
            case .token: return "access_token"
        }
    }
}

public class FacebookLoginManager: NSObject, OAuthProviderType {
    
    private let baseURLString = "https://www.facebook.com/v7.0/dialog/oauth"

    private let facebookAppID: String
    private let permissionScopes: [FbPermissionScopes]
    private let tokenType: FbTokenType
    private var view: UIViewController!
    public var providerType: AuthenticationType { return .google }
    
    public init(facebookAppID: String, permissionScopes: [FbPermissionScopes], tokenType: FbTokenType) {
        self.facebookAppID = facebookAppID
        self.permissionScopes = permissionScopes
        self.tokenType = tokenType
    }
    
    public func authenticate(from viewController: UIViewController, completion: @escaping (Result<String, OAuthAuthenticationError>) -> Void) {
        self.view = viewController
        let state = UUID().uuidString
        let callbackScheme = "fb" + facebookAppID
        let urlString = "\(baseURLString)"
            + "?client_id=\(facebookAppID)"
            + "&redirect_uri=\(callbackScheme)://authorize"
            + "&scope=\(permissionScopes.map({ $0.rawValue }).joined(separator: ","))"
            + "&response_type=\(tokenType.rawValue)%20granted_scopes"
            + "&state=\(state)"
        let url = URL(string: urlString)!
        
        let session = ASWebAuthenticationSession(url: url, callbackURLScheme: callbackScheme) { [weak self] (url, error) in
            guard let self = self else {
                completion(Result.failure(.cancelledByUser))
                return
            }
            guard error == nil else {
                if let error = error as NSError?, error.code == 1 || error.code == 0 {
                    completion(Result.failure(.cancelledByUser))
                } else {
                    completion(Result.failure(.unknown(status: error?.localizedDescription, code: (error as NSError?)?.code)))
                }
                return
            }
            guard let receivedURL = url else {
                completion(Result.failure(.cancelledByUser))
                return
            }
            guard let response = self.response(from: receivedURL) else {
                if receivedURL.absoluteString.contains("error_code=200") {
                    completion(Result.failure(.cancelledByUser))
                    return
                }
                completion(Result.failure(.unknown(status: "Unable to parse response: \(receivedURL)", code: 100)))
                return
            }
            guard response.state == state else {
                completion(Result.failure(.securityBreach))
                return
            }
            guard self.permissionScopes.compactMap({ $0.rawValue }).allSatisfy({ response.grantedPermissionScopes.contains($0) }) else {
                completion(Result.failure(.declinedPermissions))
                return
            }
            completion(Result.success(response.code))
        }
        session.presentationContextProvider = self
        session.start()
    }
    
    private func getComponent(named name: String, in items: [URLQueryItem]) -> String? {
        items.first(where: { $0.name == name })?.value
    }
    
    private func response(from url: URL) -> OAuthLoginResponse? {
        var urlToParse = url
        if tokenType == .token {
            let url = urlToParse.absoluteString.replacingOccurrences(of: "#access_token", with: "?access_token")
            urlToParse = URL(string: url)!
        }
        guard
            let items = URLComponents(url: urlToParse, resolvingAgainstBaseURL: false)?.queryItems,
            let state = getComponent(named: "state", in: items),
            let scope = getComponent(named: "granted_scopes", in: items),
            let code = getComponent(named: tokenType.tokenName, in: items)
        else { return nil }
        let grantedPermissions = scope.split(separator: ",").map(String.init)
        return OAuthLoginResponse(grantedPermissionScopes: grantedPermissions, code: code, state: state)
    }
}

extension FacebookLoginManager: ASWebAuthenticationPresentationContextProviding {
    public func presentationAnchor(for session: ASWebAuthenticationSession) -> ASPresentationAnchor {
        return view.view.window!
    }
}
