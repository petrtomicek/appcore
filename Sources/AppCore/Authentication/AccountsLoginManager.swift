import Foundation
import AuthenticationServices

public let kPropCallbackUrlScheme = "callback.url.scheme"

public protocol AccountsLoginManagerType {
    func authenticate(url: URL, from viewController: UIViewController, completion: @escaping (Result<String, OAuthAuthenticationError>) -> Void)
}

public class AccountsLoginManager: NSObject, AccountsLoginManagerType {
  
    @Dependency<String>(property: kPropCallbackUrlScheme) private var urlScheme
    
    public var providerType: AccountsAuthenticationType = .garmin
    private var view: UIViewController!

    public override init() { }
    
    public func authenticate(url: URL, from viewController: UIViewController, completion: @escaping (Result<String, OAuthAuthenticationError>) -> Void) {
        self.view = viewController
        let session = ASWebAuthenticationSession(url: url, callbackURLScheme: urlScheme) { [weak self] (url, error) in
            guard let self = self else {
                completion(Result.failure(.cancelledByUser))
                return
            }
            guard error == nil else {
                if let error = error as NSError?, error.code == 1 || error.code == 0 {
                    completion(Result.failure(.cancelledByUser))
                } else {
                    completion(Result.failure(.unknown(status: error?.localizedDescription, code: (error as NSError?)?.code)))
                }
                return
            }
            guard let receivedURL = url else {
                completion(Result.failure(.cancelledByUser))
                return
            }
            guard let response = self.response(from: receivedURL) else {
                if receivedURL.absoluteString.contains("error_code=200") {
                    completion(Result.failure(.cancelledByUser))
                    return
                }
                completion(Result.failure(.unknown(status: "Unable to parse response: \(receivedURL)", code: 100)))
                return
            }
            completion(Result.success(response.code))
        }
        session.presentationContextProvider = self
        session.start()
    }
    
  
    private func getComponent(named name: String, in items: [URLQueryItem]) -> String? {
        items.first(where: { $0.name == name })?.value
    }
    
    private func response(from url: URL) -> OAuthLoginResponse? {
        guard
            let items = URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems,
            let state = getComponent(named: "state", in: items),
            let scope = getComponent(named: "scope", in: items),
            let code = getComponent(named: "code", in: items)
        else { return nil }
        let grantedPermissions = scope.split(separator: " ").map(String.init)
        return OAuthLoginResponse(grantedPermissionScopes: grantedPermissions, code: code, state: state)
    }
}

extension AccountsLoginManager: ASWebAuthenticationPresentationContextProviding {
    public func presentationAnchor(for session: ASWebAuthenticationSession) -> ASPresentationAnchor {
        return view.view.window!
    }
}
