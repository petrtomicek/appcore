import Foundation
import UIKit

public protocol OAuthProviderType {
    var providerType: AuthenticationType { get }
    func authenticate(from viewController: UIViewController, completion: @escaping (Result<String, OAuthAuthenticationError>) -> Void)
}

public enum AuthenticationType: String {
    case google = "GOOGLE"
    case facebook = "FACEBOOK"
    case apple = "APPLE_ID"
}

public enum AccountsAuthenticationType: String {
    case oura
    case garmin
}

public enum OAuthAuthenticationError: Error {
    case cancelledByUser
    case declinedPermissions
    case unknown(status: String?, code: Int?)
    case securityBreach
}

public struct OAuthLoginResponse {
    let grantedPermissionScopes: [String]
    let code: String
    let state: String
}
