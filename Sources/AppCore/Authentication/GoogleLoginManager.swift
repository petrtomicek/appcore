import Foundation
import AuthenticationServices

public enum GooglePermissionScopes: String {
    case profile = "profile"
    case email = "email"
}

public class GoogleLoginManager: NSObject, OAuthProviderType {
    
    private let baseURLString = "https://accounts.google.com/o/oauth2/v2/auth"

    private let appId: String
    private let permissionScopes: [GooglePermissionScopes]
    private let redirectionEndpoint: URL
    private let urlScheme: String
    private var view: UIViewController!
    public var providerType: AuthenticationType { return .google }

    public init(reversedClientId: String, permissionScopes: [GooglePermissionScopes]) {
        self.permissionScopes = permissionScopes
        self.appId = reversedClientId.components(separatedBy: ".").reversed().joined(separator: ".")
        self.urlScheme = reversedClientId
        self.redirectionEndpoint = URL(string: "\(reversedClientId):/oauth2callback")!
    }
    
    public func authenticate(from viewController: UIViewController, completion: @escaping (Result<String, OAuthAuthenticationError>) -> Void) {
        self.view = viewController
        let state = UUID().uuidString
        let urlString = "\(baseURLString)"
            + "?client_id=\(appId)"
            + "&redirect_uri=\(redirectionEndpoint.absoluteString)"
            + "&scope=\(permissionScopes.map({ $0.rawValue }).joined(separator: "%20"))"
            + "&response_type=code"
            + "&state=\(state)"
            + "&access_type=offline"
        let url = URL(string: urlString)!
        
        let session = ASWebAuthenticationSession(url: url, callbackURLScheme: urlScheme) { [weak self] (url, error) in
            guard let self = self else {
                completion(Result.failure(.cancelledByUser))
                return
            }
            guard error == nil else {
                if let error = error as NSError?, error.code == 1 || error.code == 0 {
                    completion(Result.failure(.cancelledByUser))
                } else {
                    completion(Result.failure(.unknown(status: error?.localizedDescription, code: (error as NSError?)?.code)))
                }
                return
            }
            guard let receivedURL = url else {
                completion(Result.failure(.cancelledByUser))
                return
            }
            guard let response = self.response(from: receivedURL) else {
                if receivedURL.absoluteString.contains("error_code=200") {
                    completion(Result.failure(.cancelledByUser))
                    return
                }
                completion(Result.failure(.unknown(status: "Unable to parse response: \(receivedURL)", code: 100)))
                return
            }
            guard response.state == state else {
                completion(Result.failure(.securityBreach))
                return
            }
            guard self.permissionScopes.compactMap({ $0.rawValue }).allSatisfy({ response.grantedPermissionScopes.contains($0) }) else {
                completion(Result.failure(.declinedPermissions))
                return
            }
            self.exchangeCodeForAccessToken(response.code, completion: completion)
        }
        session.presentationContextProvider = self
        session.start()
    }
    
    private func exchangeCodeForAccessToken(_ authorizationCode: String, completion: @escaping (Result<String, OAuthAuthenticationError>) -> Void) {
        let session = URLSession(configuration: .ephemeral)
        let url = URL(string: "https://www.googleapis.com/oauth2/v4/token")!
        let requestParams: [String: String?] = [
            "client_id": appId,
            "code": authorizationCode,
            "grant_type": "authorization_code",
            "redirect_uri": redirectionEndpoint.absoluteString
        ]
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = GoogleLoginManagerHelper.queryString(requestParams)?.data(using: String.Encoding.utf8)
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            guard
                let data = data,
                let json = (try? JSONSerialization.jsonObject(with: data, options: [])) as? [String: Any],
                let accessToken = json["access_token"] as? String
            else {
                completion(Result.failure(.unknown(status: error?.localizedDescription, code: (error as NSError?)?.code)))
                return
            }
            completion(Result.success(accessToken))
        }
        task.resume()
    }
    
    private func getComponent(named name: String, in items: [URLQueryItem]) -> String? {
        items.first(where: { $0.name == name })?.value
    }
    
    private func response(from url: URL) -> OAuthLoginResponse? {
        guard
            let items = URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems,
            let state = getComponent(named: "state", in: items),
            let scope = getComponent(named: "scope", in: items),
            let code = getComponent(named: "code", in: items)
        else { return nil }
        let grantedPermissions = scope.split(separator: " ").map(String.init)
        return OAuthLoginResponse(grantedPermissionScopes: grantedPermissions, code: code, state: state)
    }
}

extension GoogleLoginManager: ASWebAuthenticationPresentationContextProviding {
    public func presentationAnchor(for session: ASWebAuthenticationSession) -> ASPresentationAnchor {
        return view.view.window!
    }
}

public class GoogleLoginManagerHelper {
    
    static func registeredURLSchemes(bundle: Bundle, filter completion: (String) -> Bool) -> [String] {
        guard let urlTypes = bundle.infoDictionary?["CFBundleURLTypes"] as? [[String: AnyObject]] else {
            return [String]()
        }
        let urlSchemes = urlTypes.compactMap({($0["CFBundleURLSchemes"] as? [String])?.first })
        return urlSchemes.compactMap({ completion($0) ? $0 : nil})
    }
    
    static func queryString(_ parts: [String: String?]) -> String? {
        return parts.compactMap { key, value -> String? in
            if let value = value {
                return key + "=" + value
            } else {
                return nil
            }
        }.joined(separator: "&").addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    }
}
