import Foundation

public extension Int {

    var formattedWithAbbreviation: String {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 1
        var factor = 0
        let tokens = ["", "k", "M", "G"]
        var value = Double(self)
        while value > 1000 {
            value /= 1000
            factor += 1
        }
        return "\(formatter.string(from: value as NSNumber) ?? "N/A")\(tokens[factor])"
    }
    
   /* var ordinalSuffix: String {
        let ones: Int = self % 10
        let tens: Int = (self/10) % 10
        if tens == 1 {
            return Strings.ordinalNumber4
        } else if ones == 1 {
            return Strings.ordinalNumber1
        } else if ones == 2 {
            return Strings.ordinalNumber2
        } else if ones == 3 {
            return Strings.ordinalNumber3
        } else {
            return Strings.ordinalNumber4
        }
        //OS way
        //let numberFormatter = NumberFormatter()
        //numberFormatter.numberStyle = .ordinal
        //return numberFormatter.string(from: NSNumber(value: self)) ?? ""
    }*/
    
    init(_ string: String?) {
        if let string = string {
            self = .init(string) ?? 0
        } else {
            self = 0
        }
    }
}
