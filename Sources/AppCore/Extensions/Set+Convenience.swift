import Foundation

public extension Set {
    
    var isNotEmpty: Bool {
        return !isEmpty
    }
    
    /// If element is presented in Set, it will removes it, otherwise it is inserted
    mutating func toggle(_ element: Element) {
        if contains(element) {
            remove(element)
        } else {
            insert(element)
        }
    }
}
