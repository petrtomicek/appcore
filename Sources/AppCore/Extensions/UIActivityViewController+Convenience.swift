import UIKit

extension UIActivityViewController {
    
    public convenience init(title: String, url: String) {
        let activityTitle = title
        
        // MARK: Hack for copying sharable text + link to Messenger, WhatsUp etc.
        var activityItems: [Any] = [activityTitle.trimmingCharacters(in: CharacterSet(charactersIn: " ")) + " "]
        
        if let sharableUrl = URL(string: url) {
            activityItems.append(sharableUrl)
        }
        
        self.init(activityItems: activityItems, applicationActivities: nil)
        
        self.setValue(title, forKey: "subject")
    }
}
