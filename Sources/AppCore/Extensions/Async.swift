import Foundation

/**
 Please read the following documetation from Apple:
 https://developer.apple.com/library/prerelease/ios/documentation/Performance/Conceptual/EnergyGuide-iOS/PrioritizeWorkWithQoS.html
 These helper functions just wrap dispatching to specific Quality of Service (QOS) queues. These should
 be used according to the specifications mentioned by apple.
 */

public func asyncMain(after delay: Double? = nil, closure: @escaping () -> Void) {
    async(
        on: DispatchQueue.main,
        after: delay,
        closure: closure
    )
}

public func asyncUserInteractive(after delay: Double? = nil, closure: @escaping () -> Void) {
    async(
        on: DispatchQueue.global(qos: .userInteractive),
        after: delay,
        closure: closure
    )
}

public func asyncUserInitiated(after delay: Double? = nil, closure: @escaping () -> Void) {
    async(
        on: DispatchQueue.global(qos: .userInitiated),
        after: delay,
        closure: closure
    )
}

public func asyncUtility(after delay: Double? = nil, closure: @escaping () -> Void) {
    async(
        on: DispatchQueue.global(qos: .utility),
        after: delay,
        closure: closure
    )
}

public func asyncBackground(after delay: Double? = nil, closure: @escaping () -> Void) {
    async(
        on: DispatchQueue.global(qos: .background),
        after: delay,
        closure: closure
    )
}

public func async(on queue: DispatchQueue, after delay: Double?, closure: @escaping () -> Void) {
    if let delay = delay, delay > 0 {
        queue.asyncAfter(deadline: DispatchTime.now() + .milliseconds(Int(delay * 1000)), execute: closure)
    } else {
        queue.async(execute: closure)
    }
}

/**
 Will create an async timer that can run on a background queue. It is important to note that the block will be owned so
 you must use [weak self]
 
 - parameter interval: the timer interval in seconds
 - parameter queue:    the queue to run it on
 - parameter block:    the block to call when the timer fires
 
 - returns: the timer, you must cancel this timer using dispatch_source_cancel(timer)
 */
public func asyncTimer(_ interval: Double,
                queue: DispatchQueue? = DispatchQueue.global(qos: DispatchQoS.QoSClass.utility),
                block: @escaping () -> Void) -> DispatchSourceTimer {
    let timer = DispatchSource.makeTimerSource(flags: DispatchSource.TimerFlags(rawValue: 0), queue: queue)
    timer.schedule(deadline: DispatchTime.now() + DispatchTimeInterval.seconds(Int(interval)),
                   repeating: DispatchTimeInterval.seconds(Int(interval)),
                   leeway: DispatchTimeInterval.milliseconds(100))
    timer.setEventHandler(handler: DispatchWorkItem(block: block))
    timer.resume()
    return timer
}
