import UIKit

public extension UIApplication {

    class func openAppSettings(closure: @escaping (Bool) -> Void) {
        let settingsUrl = URL(string: UIApplication.openSettingsURLString)!
        if UIApplication.shared.canOpenURL(settingsUrl) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(settingsUrl, options: [:], completionHandler: closure)
            } else {
                UIApplication.shared.openURL(settingsUrl)
            }
        } else {
            closure(false)
        }
    }
}
