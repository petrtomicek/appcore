import UIKit

public extension UIAlertController {

    convenience init(alertTitle: String?, message: String? = nil) {
        self.init(title: alertTitle, message: message, preferredStyle: .alert)
    }

    convenience init(actionSheetTitle: String?) {
        self.init(title: actionSheetTitle, message: nil, preferredStyle: .actionSheet)
    }

    convenience init(alertViewModel viewModel: AlertViewModel) {
        self.init(alertTitle: viewModel.title, message: viewModel.message)
        addActions(viewModel.actions)
    }

    convenience init(actionSheetViewModel viewModel: ActionSheetViewModel) {
        self.init(actionSheetTitle: viewModel.title)
        addActions(viewModel.actions)
    }

    private func addActions(_ actions: [AlertAction]) {
        actions.forEach { action in
            addAction(
                action.title,
                style: action.style,
                isPreferred: action.isPreferred,
                isEnabled: action.isEnabled
            ) { _ in
                action.action?()
            }
        }
    }
    
    @discardableResult
    func addAction(_ title: String,
                   style: UIAlertAction.Style,
                   isPreferred: Bool = false,
                   isEnabled: Bool = true,
                   action: ((UIAlertAction) -> Void)? = nil) -> UIAlertAction {
        let action = UIAlertAction(title: title, style: style, handler: action)
        action.isEnabled = isEnabled
        addAction(action)
        if isPreferred {
            preferredAction = action
        }
        return action
    }
}

public struct AlertAction {
    public let title: String
    public let style: UIAlertAction.Style
    public let isPreferred: Bool
    public let isEnabled: Bool
    public let action: (() -> Void)?

    public init(title: String,
                style: UIAlertAction.Style,
                isPreferred: Bool = false,
                isEnabled: Bool = true,
                action: (() -> Void)? = nil) {
        self.title = title
        self.style = style
        self.isPreferred = isPreferred
        self.isEnabled = isEnabled
        self.action = action
    }
}

public struct AlertViewModel {
    public let iconImage: UIImage?
    public let title: String?
    public let message: String?
    public let actions: [AlertAction]

    public init(title: String?, message: String?, actions: [AlertAction], iconImage: UIImage? = nil) {
        self.iconImage = iconImage
        self.title = title
        self.message = message
        self.actions = actions
    }
}

public struct ActionSheetViewModel {
    public let title: String?
    public let message: String?
    public let actions: [AlertAction]

    public init(title: String?, message: String?, actions: [AlertAction]) {
        self.title = title
        self.message = message
        self.actions = actions
    }
}
