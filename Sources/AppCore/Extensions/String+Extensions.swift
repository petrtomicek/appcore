import Foundation

public extension String {

    var doubleString: String {
        return self.replacingOccurrences(of: ",", with: ".")
    }

    var doubleValue: Double? {
        return Double(doubleString)
    }

    var doubleOrZeroValue: Double {
        return Double(doubleString) ?? 0
    }

    func notContains(_ value: String?) -> Bool {
        guard let value = value else { return false }
        return !contains(value)
    }

    var capitalizingFirstLetter: String {
        return prefix(1).capitalized + dropFirst().lowercased()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter
    }

    var withRegularCharacters: String {
        return String(unicodeScalars.filter({ $0.utf8.count == 1 }))
        //let set = CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890- ._+")
        //return String(unicodeScalars.filter(set.contains))
    }

    var isNumber: Bool {
        return CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: self))
    }

    var isLettersOnly: Bool {
        return CharacterSet.letters.isSuperset(of: CharacterSet(charactersIn: self))
    }

    /// Removes <p></p> tags from string
    func removingLeadingHtmlParagraph() -> String {
        let contentRegex = "\\<p(.*?)\\>(.*?)<\\/p>"
        var paragraphs: [String] = []
        let tagRegex = try? NSRegularExpression(pattern: contentRegex, options: .caseInsensitive)
        let tagRanges = tagRegex?.matches(in: self, range: NSRange(0..<self.count)) ?? []
        for range in tagRanges {
            guard range.numberOfRanges > 1 else { continue }
            let contentRange = range.range(at: 2)
            let content = NSString(string: self).substring(with: contentRange)
            paragraphs.append(content)
        }
        return paragraphs.isEmpty ? self : paragraphs.joined(separator: "<p></p>")
    }

    var isUrlLink: Bool {
        return starts(with: "http")
    }

    subscript(bounds: CountableClosedRange<Int>) -> String {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return String(self[start...end])
    }

    subscript(bounds: CountableRange<Int>) -> String {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return String(self[start..<end])
    }

    func contains(insensitive other: String, range: Range<String.Index>? = nil, locale: Locale? = nil) -> Bool {
        return self.range(of: other, options: [.diacriticInsensitive, .caseInsensitive], range: range, locale: locale) != nil
    }

    var rawFileName: String {
        let components = self.components(separatedBy: ".")
        if components.count < 2 {
            return self
        }
        return components.take(components.count - 1).joined()
    }
}

public extension Optional where Wrapped == String {
    
    var doubleString: String {
        return self?.replacingOccurrences(of: ",", with: ".") ?? ""
    }
    
    var doubleValue: Double? {
        return Double(doubleString)
    }
    
    var doubleOrZeroValue: Double {
        return Double(doubleString) ?? 0
    }
}
