import UIKit

public extension UIView {

    func addSubviews(_ views: UIView...) {
        views.forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            addSubview($0)
        }
    }

    func pinSubview(_ subview: UIView, insets: UIEdgeInsets = .zero) {
        addSubviews(subview)
        NSLayoutConstraint.activate([
            subview.leadingAnchor.constraint(equalTo: leadingAnchor, constant: insets.left),
            subview.topAnchor.constraint(equalTo: topAnchor, constant: insets.top),
            subview.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -insets.right),
            subview.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -insets.bottom)
        ])
    }
    
    func pinSubviews(_ views: UIView..., insets: UIEdgeInsets = .zero) {
        views.forEach {
            pinSubview($0, insets: insets)
        }
    }

    func recursiveSuperviews() -> DropFirstSequence<UnfoldSequence<UIView, (UIView?, Bool)>> {
        return sequence(first: self, next: { $0.superview }).dropFirst(1)
    }
    
    func removeSubviews() {
        subviews.forEach({ $0.removeFromSuperview() })
    }

    func superview<T: UIView>(type: T.Type) -> T? {
        return recursiveSuperviews().first(where: { $0 is T }) as? T
    }

    func startRotatingAnimation(_ duration: Double = 1) {
        let kAnimationKey = "rotation"
        if self.layer.animation(forKey: kAnimationKey) != nil {
            self.layer.removeAnimation(forKey: kAnimationKey)
        }
        let animate = CABasicAnimation(keyPath: "transform.rotation")
        animate.duration = duration
        animate.repeatCount = Float.infinity
        animate.fromValue = 0.0
        animate.toValue = Float(.pi * 2.0)
        DispatchQueue.main.async {
            self.layer.add(animate, forKey: kAnimationKey)
        }
    }

    func stopRotatingAnimation() {
        let kAnimationKey = "rotation"
        if self.layer.animation(forKey: kAnimationKey) != nil {
            self.layer.removeAnimation(forKey: kAnimationKey)
        }
    }
    
    func animateIsHidden(_ hidden: Bool, duration: TimeInterval) {
        if self.isHidden && !hidden {
            self.alpha = 0.0
            self.isHidden = false
        }
        UIView.animate(withDuration: duration, animations: {
            self.alpha = hidden ? 0.0 : 1.0
        }) { (complete) in
            self.isHidden = hidden
        }
    }
    
    func rotate(degrees: CGFloat) {
        transform = CGAffineTransform(rotationAngle: degrees * .pi / 180)
    }
    
    func rotate(degrees: CGFloat, duration: CFTimeInterval, repeatCount: Float = Float.greatestFiniteMagnitude) {
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 2
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        self.layer.add(rotation, forKey: "rotationAnimation")
    }
    
    func applyBorder(width: CGFloat, color: UIColor) {
        layer.borderWidth = width
        layer.borderColor = color.cgColor
    }
    
    func round(corners: UIRectCorner, radius: CGFloat) {
        layer.masksToBounds = true
        layer.cornerRadius = radius
        layer.maskedCorners = CACornerMask(rawValue: corners.rawValue)
    }

    func applyCornerRadius(_ radius: CGFloat) {
        layer.cornerRadius = radius
        clipsToBounds = true
    }
}

public extension UIStackView {

    func addArrangedSubviews(_ views: UIView...) {
        views.forEach {
            addArrangedSubview($0)
        }
    }
}
