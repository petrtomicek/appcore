import Foundation

public extension Bool {

    var isFalse: Bool {
        return self == false
    }
}

public extension Optional where Wrapped == Bool {

    var defaultTrue: Bool {
        guard let self else { return true }
        return self
    }

    var defaultFalse: Bool {
        guard let self else { return false }
        return self
    }
}
