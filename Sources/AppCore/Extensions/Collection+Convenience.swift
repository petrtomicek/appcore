import Foundation

public extension Collection {
    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript(safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }

    /// Returns second element from collection
    var second: Element? {
        return dropFirst().first
    }

    /// Use this for checks instead of !.isEmpty that is triggered as warning by Swiftlint
    var isNotEmpty: Bool {
        return !isEmpty
    }
}

public extension Array {

    /// returns index of last component in array
    var lastIndex: Int {
        return Swift.max(0, count - 1)
    }
}

public extension Sequence {
    
    func pairs() -> AnyIterator<(Element, Element?)> {
        return AnyIterator(sequence(state: makeIterator(), next: { it in
            it.next().map { ($0, it.next()) }
        })
        )
    }
}
