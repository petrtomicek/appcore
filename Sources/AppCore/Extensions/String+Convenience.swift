import UIKit

public extension String {

    var length: Int {
        return utf16.count
    }
}

public extension Optional where Wrapped == String {

    /// Replacement for `string?.isEmpty ?? true`
    var isEmptyOrNil: Bool {
        guard let value = self else { return true }
        return value.isEmpty
    }

    var isNotEmptyOrNil: Bool {
        guard let value = self else { return false }
        return !value.isEmpty
    }

    /// Replacement for `string?.isEmpty != false ? nil : string!`
    var nilIfEmpty: String? {
        guard let value = self else { return nil }
        return value.isEmpty ? nil : value
    }
    
    var nilIfEmptyOrWhitespace: String? {
        guard let value = self else { return nil }
        return (value.isEmpty || value == " ") ? nil : value
    }

    /// Replacement for `string ?? ""`
    var emptyIfNil: String {
        guard let value = self else { return "" }
        return value
    }
    
    /// Returns whitespace string if empty or nil
    var emptyWhitespaceIfNilOrEmpty: String {
        guard let value = self, value.isNotEmpty else { return " " }
        return value
    }
}

public extension String {

    func animationName(_ darkModeEnabled: Bool) -> String {
        guard darkModeEnabled else { return self }
        return self + "DarkMode"
    }
    
    var first: String {
        return String(prefix(1))
    }

    var last: String {
        return String(suffix(1))
    }

    var uppercaseFirstCharacterString: String {
        return first.uppercased() + String(dropFirst())
    }

    func sizeFor(font: UIFont, lines: Int = 1) -> CGSize {
        let label = UILabel(frame: .zero)
        label.text = self
        label.numberOfLines = lines
        label.font = font
        label.sizeToFit()
        return label.frame.size
    }
    
    func sizeFor(font: UIFont, width: CGFloat) -> CGSize {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: 0))
        label.text = self
        label.numberOfLines = 0
        label.font = font
        label.sizeToFit()
        return label.frame.size
    }

    /// Replacement for `string.isEmpty ? nil : string`
    var nilIfEmpty: String? {
        return isEmpty ? nil : self
    }

    /**
     Will trim the whitespace from both ends of a string, including new line characters.
     - returns: the string with the whitespace and new line characters trimmed
     */
    func trim() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }

    /**
     This will remove the excessive characters from the end of the string
     - parameter maxLength: maximum length of the string to be shown
     - returns: trimmed string
     */
    func wrap(_ maxLength: Int) -> String {
        let length = self.count
        if length <= maxLength {
            return self
        }

        let end = self.index(self.startIndex, offsetBy: maxLength)
        return String(self[..<end])
    }

    /**
     Generates radom string which is not cryptographically strong.
     */
    static func random(_ length: Int = 6) -> String {
        let base = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString: String = ""
        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.count))
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        return randomString
    }

    var boolValue: Bool {
        return (self as NSString).boolValue
    }

    /**
     Remove any non-digit characters from the string
     */
    func stripToDigits() -> String {
        return self.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    }

    /**
     Extract array of URLs from the string
     */
    func extractURLs() -> [(url: URL, range: NSRange)] {
        var urls: [(URL, NSRange)] = []
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            detector.enumerateMatches(in: self, options: [], range: NSRange(location: 0, length: self.count), using: { (result, _, _) in
                if let url = result?.url, let range = result?.range {
                    urls.append((url, range))
                }
            })
        } catch { }
        return urls
    }
}
