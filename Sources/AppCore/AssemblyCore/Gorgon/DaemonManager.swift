/***************************************************************************************************
 * Forked from https://github.com/locationlabs/Gorgon
 * Author: LocationLabs
 * Licenced under Apache License 2.0.
 * Full text of the licence is at https://github.com/locationlabs/Gorgon/blob/master/LICENSE
 *
 **************************************************************************************************/

import UIKit

/**
 The Daemon manages the DaemonType's that are registered. The Daemon is a accessed via the sharedInstance and thus
 will live for the life of the application once a daemon has been registered. Daemons are meant to be long lived and
 respond to system events
 */
public final class DaemonManager {
    
    /// list of daemons for the application
    fileprivate var daemons = [DaemonType]()
        
    public init() {
        self.registerLifecycleNotifications()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    /**
     Will register a daemon with the VIPER application
     
     - parameter daemon: the daemon to register
     */
    public func register(_ daemon: DaemonType) {
        daemons.append(daemon)
        
        // setup notification handlers for daemon
        if let notificationCenterDaemon = daemon as? NotificationCenterDaemonType {
            for (notificationName, selector) in notificationCenterDaemon.notificationToSelectors {
                NotificationCenter.default
                    .addObserver(daemon,
                                 selector: selector,
                                 name: NSNotification.Name(rawValue: notificationName),
                                 object: nil)
            }
        }
    }
    
    /**
     Will retrieve daemons for a given type
     
     - parameter type: the type of daemon
     
     - returns: the daemons that implement the given type
     */
    public func daemonsForType<T>(_ type: T.Type) -> [T] {
        return daemons.map { $0 as? T }.compactMap { $0 }
    }
}

// MARK: - Singleton
public extension DaemonManager {
    static let shared = DaemonManager()
}

// MARK: - URL
public extension DaemonManager {
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        let sourceApplication = options[.sourceApplication] as? String
        logDebug("Application open url, url=\(url.path), query=\(String(describing: url.query)), scheme=\(String(describing: url.scheme)),"
                    + " host=\(String(describing: url.host)), path=\(url.path), sourceApplication=\(String(describing: sourceApplication))")

        let _url = Url(url: url, sourceApplication: sourceApplication)
        for daemon in daemonsForType(UrlDaemonType.self) {
            if let fragments = _url.matchUrlPathAndBuildFragments(daemon.urlHost, path: daemon.urlPath) {
                if daemon.handleUrl(url, fragments: fragments, queryParams: _url.paramsForQueryString()) {
                    return true
                }
            }
        }
        for daemon in daemonsForType(DynamicLinkDaemonType.self) {
            if daemon.application(app, open: url, options: options) {
                return true
            }
        }
        return false
    }
}

// MARK: - Continuing User Activity and handling Universal Link
public extension DaemonManager {
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        return daemonsForType(DynamicLinkDaemonType.self).first {
            $0.application(application, continue: userActivity, restorationHandler: restorationHandler)
        } != nil
    }

    func application(handle userActivity: NSUserActivity) {
        daemonsForType(DynamicLinkDaemonType.self).forEach {
            $0.application(handle: userActivity)
        }
    }
}

// MARK: - Local Notifications
public extension DaemonManager {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        let category = response.notification.request.content.categoryIdentifier
        logDebug("Application did receive local notification, category=\(category),"
            + " userInfo=\(String(describing: userInfo))")

        for daemon in daemonsForType(LocalNotificationDaemonType.self) {
            if daemon.category == category {
                daemon.handleNotification(userInfo as? [String: AnyObject])
                return
            }
        }
        logDebug("Application local notification daemon not found, category=\(category),"
            + " userInfo=\(String(describing: userInfo))")
    }
    
    func handleAction(userInfo: [String: AnyObject]?, completionHandler: @escaping () -> Void) {
        // guard let category = notification.category else { return }
        for daemon in daemonsForType(LocalActionNotificationDaemonType.self) {
            daemon.handleActionWithIdentifier(nil, userInfo: userInfo, completionHandler: completionHandler)
        }
    }
}

// MARK: - APNS/VoIP Notifications
public extension DaemonManager {
    /**
     Should be invoked by the APNS or VoIP delegate hook for push token registration
     - parameter token: the push token
     */
    func deviceTokenRegistered(_ token: String) {
        logDebug("Application device token registered, token=\(token)")
        for daemon in DaemonManager.shared.daemonsForType(DeviceDaemonType.self) {
            daemon.deviceTokenRegistered(token)
        }
    }

    func handleDisplayNotificationInApp(notification: UNNotification) {
        for daemon in DaemonManager.shared.daemonsForType(RemoteNotificationDaemonType.self) {
            daemon.handleDisplayNotificationInApp(notification: notification)
        }
    }

    /**
     Should be invoked by the APNS or VoIP delegates when a push notification is handled by the application. This will
     look up the correct daemon to handle the push notification
     
     - parameter userInfo:   the payload of the push
     - parameter source:     the original receiver of remote notification
     - parameter completion: the completion callback
     */
    func handleRemoteNotification(_ userInfo: [AnyHashable: Any], source: RemoteNotificationSource, response: UNNotificationResponse? = nil, notification: UNNotification? = nil, completion: @escaping (UIBackgroundFetchResult) -> Void) {
        if let userInfo = userInfo as? [String: AnyObject] {
            // Pushes from legacy systems may not have a category.  We support them
            // with this constant.  Only one daemon can have no category.
//            var category = kRemoteNotificationNoCategory
//            if let aps = userInfo["aps"] as? [String: AnyObject],
//                let categoryFromPush = aps["category"] as? String {
//                category = categoryFromPush
//            }
            // find daemon to handle push
            for daemon in DaemonManager.shared.daemonsForType(RemoteNotificationDaemonType.self) {
                daemon.handleNotification(userInfo, source: source, response: response, notificationSource: notification, completion: completion)
            }
        }
    }
    
    func handleInvalidatePushToken() {
        for daemon in DaemonManager.shared.daemonsForType(RemoteNotificationErrorDaemonType.self) {
                daemon.didInvalidatePushToken()
        }
    }
}

// MARK: - Notifications for Application Lifecycle
public extension DaemonManager {
    fileprivate func registerLifecycleNotifications() {
        NotificationCenter.default
            .addObserver(self, selector: #selector(onApplicationDidFinishLaunching(_:)),
                         name: UIApplication.didFinishLaunchingNotification, object: nil)
        NotificationCenter.default
            .addObserver(self, selector: #selector(onApplicationWillResignActive(_:)),
                         name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default
            .addObserver(self, selector: #selector(onApplicationDidEnterBackground(_:)),
                         name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default
            .addObserver(self, selector: #selector(onApplicationWillEnterForeground(_:)),
                         name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default
            .addObserver(self, selector: #selector(onApplicationDidBecomeActive(_:)),
                         name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default
            .addObserver(self, selector: #selector(onApplicationWillTerminate(_:)),
                         name: UIApplication.willTerminateNotification, object: nil)
    }
    
    @objc func onApplicationDidFinishLaunching(_ notification: Notification) {
        let launchOptions = notification.userInfo?[UIApplication.LaunchOptionsKey.userActivityDictionary] as? [AnyHashable: Any]
        logDebug("Application did Finish launching with options, options=\(String(describing: launchOptions))")
        for daemon in daemonsForType(ApplicationDaemonType.self) {
            daemon.application(UIApplication.shared, didFinishLaunchingWithOptions: launchOptions)
        }
    }
    
    @objc func onApplicationWillResignActive(_ notification: Notification) {
        logDebug("Application will resign active")
        for daemon in daemonsForType(ApplicationDaemonType.self) {
            daemon.applicationWillResignActive(UIApplication.shared)
        }
    }
    
    @objc func onApplicationDidEnterBackground(_ notification: Notification) {
        logDebug("Application did enter background")
        for daemon in daemonsForType(ApplicationDaemonType.self) {
            daemon.applicationDidEnterBackground(UIApplication.shared)
        }
    }
    
    @objc func onApplicationWillEnterForeground(_ notification: Notification) {
        logDebug("Application will enter foreground")
        for daemon in daemonsForType(ApplicationDaemonType.self) {
            daemon.applicationWillEnterForeground(UIApplication.shared)
        }
    }
    
    @objc func onApplicationDidBecomeActive(_ notification: Notification) {
        logDebug("Application did become active")
        for daemon in daemonsForType(ApplicationDaemonType.self) {
            daemon.applicationDidBecomeActive(UIApplication.shared)
        }
    }
    
    @objc func onApplicationWillTerminate(_ application: UIApplication) {
        logDebug("Application will terminate")
        for daemon in daemonsForType(ApplicationDaemonType.self) {
            daemon.applicationWillTerminate(application)
        }
    }
}
