/***************************************************************************************************
 * Forked from https://github.com/locationlabs/Gorgon
 * Author: LocationLabs
 * Licenced under Apache License 2.0.
 * Full text of the licence is at https://github.com/locationlabs/Gorgon/blob/master/LICENSE
 *
 **************************************************************************************************/

import Foundation

public final class Log {
    static var enableDebugging = false
}

public func logDebug(_ message: @autoclosure () -> String) {
    if Log.enableDebugging {
        NSLog("[Gorgon] \(message())")
    }
}
