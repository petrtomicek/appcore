/***************************************************************************************************
 * Forked from https://github.com/locationlabs/Gorgon
 * Author: LocationLabs
 * Licenced under Apache License 2.0.
 * Full text of the licence is at https://github.com/locationlabs/Gorgon/blob/master/LICENSE
 *
 **************************************************************************************************/

import UIKit

public protocol DynamicLinkDaemonType: DaemonType {
    /**
     Received when the app is opened for the first time after installation.
     */
    func application(_ application: UIApplication,
                     open url: URL,
                     options: [UIApplication.OpenURLOptionsKey: Any]) -> Bool
    
    /**
     Received when the app is already installed to handle Universal Link.
     */
    func application(_ application: UIApplication,
                     continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool

    /**
     Received when the app is already installed to handle Universal Link using scene.
     */
    func application(handle userActivity: NSUserActivity)
}
