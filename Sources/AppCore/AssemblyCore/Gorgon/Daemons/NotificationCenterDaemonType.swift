/***************************************************************************************************
 * Forked from https://github.com/locationlabs/Gorgon
 * Author: LocationLabs
 * Licenced under Apache License 2.0.
 * Full text of the licence is at https://github.com/locationlabs/Gorgon/blob/master/LICENSE
 *
 **************************************************************************************************/

import Foundation

/**
 The NotificationCenterDaemonType to handle notifications from the default NSNotificationCenter
 */
public protocol NotificationCenterDaemonType: DaemonType {
    
    /**
     Name of the notifications mapped to their selector handlers
     
     ```
     var notificationToSelectors = [
         "FooNotification": #selector(doFooNotification(_:))
     ]
     ```
     */
    var notificationToSelectors: [String: Selector] { get }
}
