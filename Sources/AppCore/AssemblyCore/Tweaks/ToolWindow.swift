import Foundation
import UIKit

open class ToolWindow: UIWindow {
    
    public struct Tool {
        let name: String
        let isShown: () -> Bool
        let show: () -> Void
        public init(
            name: String,
            isShown: @escaping () -> Bool,
            show: @escaping () -> Void
            ) {
            self.name = name
            self.isShown = isShown
            self.show = show
        }
    }
    
    public var tools: [Tool] = []
    
    private var shaking = false
    private var presented = false
    private let delay = 0.4
    
    open override func motionBegan(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        #if !AppStore
        if motion == .motionShake {
            shaking = true
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delay) {
                [weak self] in
                guard let self = self else { return }
                #if targetEnvironment(simulator)
                self.showTools()
                #else
                if self.shaking {
                    self.showTools()
                }
                #endif
            }
        }
        #endif
        super.motionBegan(motion, with: event)
    }
    
    open override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        #if !AppStore
        if motion == .motionShake {
            shaking = false
        }
        #endif
        super.motionEnded(motion, with: event)
    }
    
    private func showTools() {
        guard !presented, !tools.lazy.map({ $0.isShown() }).contains(where: { $0 }) else { return }
        presented = true
        let alert = UIAlertController(title: "Tools", message: nil, preferredStyle: .actionSheet)
        alert.addAction(
            UIAlertAction(
                title: "Dismiss",
                style: .cancel,
                handler: { [weak self] _ in
                    self?.presented = false
                }
            )
        )
        let delay = self.delay
        for tool in tools {
            alert.addAction(
                UIAlertAction(
                    title: tool.name,
                    style: .default,
                    handler: { [weak self] _ in
                        self?.presented = false
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delay) {
                            tool.show()
                        }
                    }
                )
            )
        }
        
        var visibleViewController = rootViewController
        while let vc = visibleViewController?.presentedViewController {
            visibleViewController = vc
        }
        
        visibleViewController?.present(alert, animated: true, completion: nil)
    }
}
