/***************************************************************************************************
 * Forked from https://github.com/locationlabs/Cobra
 * Author: LocationLabs
 * Licenced under Apache License 2.0.
 * Full text of the licence is at https://github.com/locationlabs/Cobra/blob/master/LICENSE
 *
 **************************************************************************************************/

import Swinject
import UIKit

public protocol ModuleBaseType: AnyObject {}
public protocol ModuleDelegateType: AnyObject {}
public protocol ModuleDelegating: AnyObject {}
public protocol ModuleModelType: AnyObject {}

public protocol ModuleAssemblyType: AnyObject, Constructible, Assembly {
    func registerView(in: Container)
    func registerPresenter(in: Container)
    func registerInteractor(in: Container)
    func registerModule(in: Container)
}

public protocol CoordinatorType: AnyObject {
    func start()
}

public protocol ViewType: AnyObject {}

public protocol CoordinatorModelType: AnyObject {}
public protocol CoordinatorDelegateType: AnyObject {
    func display(viewController: UIViewController, source: CoordinatorType)
}

public protocol CoordinatorAssemblyType: AnyObject, Constructible, Assembly {
    func registerCoordinator(in: Container)
}

public protocol PresenterViewType: AnyObject {}
public protocol PresenterInteractorType: AnyObject {}
public protocol PresenterType: PresenterViewType, PresenterInteractorType {}
public protocol InteractorType: AnyObject {}
