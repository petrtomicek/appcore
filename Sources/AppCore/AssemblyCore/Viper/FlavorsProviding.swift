import Foundation

/**
 Protocol represents a provider of flavors
 This protocol should be implemented by top level app if it needs to override
 default flavor and extend supported flavors
 */
public protocol FlavorsProviding {
    /**
     If an appdelegate defines default flavor, then it should provide it.
     At least one default flavor should be defined
     */
    var defaultFlavor: Flavor? { get }

    /**
     If an appdelegate defines supported flavors, then it should provide it
     */
    var supportedFlavors: [Flavor] { get }

    /**
     If an appdelegate defines supported flavors, then it should provide also flavor properties
     */
    var flavorProperties: [PropertyType] { get }
}

public extension FlavorsProviding {
    var defaultFlavor: Flavor? { return nil }

    var flavorProperties: [PropertyType] {
        if let flavor = supportedFlavors.productionFlavor {
            #if AppStore
            return [flavor.jsonProperty]
            #endif
        }
        return supportedFlavors.map { $0.jsonProperty }
    }
}

public extension Array where Element == FlavorsProviding {
    var defaultFlavor: Flavor {
        guard let flavor = compactMap({ $0.defaultFlavor }).last else {
            fatalError("You have to define your default flavor!")
        }
        return flavor
    }

    var supportedFlavors: [Flavor] {
        var allFlavors = [Flavor]()

        flatMap { $0.supportedFlavors }.forEach {
            if !allFlavors.contains($0) {
                allFlavors.append($0)
            }
        }
        return allFlavors
    }

    var flavorProperties: [PropertyType] {
        return flatMap { $0.flavorProperties }
    }
}

private extension Array where Element == Flavor {
    var productionFlavor: Flavor? {
        return first(where: { $0 == .Production })
    }
}
