import Foundation
import Swinject

///Every optional feature is implemented and configured using its implementation of `AssembliesProviding`
public protocol AssembliesProviding: Constructible {
    /**
     If an assembly has json properties it should implement this property by loading
     its set of properties from json file.
     */
    var properties: [PropertyType] { get }

    /**
     If an assembly has components it should implement this property to get it properly propagated
     */
    var components: [ComponentType] { get }

    /**
     If an assembly provides extra overrides for existing modules, it should provide those modules
     in this property
     */
    var proxies: [ProxyType] { get }
}

public extension AssembliesProviding {
    /// Default implementation for assemblies which do not have json properties
    var properties: [PropertyType] { return [] }
    /// Default implementation for assemblies which do not have components
    var components: [ComponentType] { return [] }
    /// Default implementation for assemblies which do not have proxies
    var proxies: [ProxyType] { return [] }
}
