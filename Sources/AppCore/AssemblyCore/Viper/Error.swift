/***************************************************************************************************
 * Forked from https://github.com/locationlabs/Cobra
 * Author: LocationLabs
 * Licenced under Apache License 2.0.
 * Full text of the licence is at https://github.com/locationlabs/Cobra/blob/master/LICENSE
 *
 **************************************************************************************************/

/**
 Represents errors that can be thrown by the VIPER framework
 
 - MissingFeature:      Attempting to load a feature that does not exist in the container
 - MissingProxy:        Adding to load a feature that is not registered via a proxy
 - MissingModule:       Attempting to proxy to a module whose key is not associated with the Proxy
 */

public enum ViperError: Error {

    case missingFeature
    case missingProxy
    case missingModule
}
