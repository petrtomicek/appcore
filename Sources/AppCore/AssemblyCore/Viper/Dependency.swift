import Foundation
import Swinject

@propertyWrapper
public struct Dependency<T> {
    
    internal let property: String?
    
    public var wrappedValue: T {
        guard let property = property else {
            return resolve()
        }
        return resolve(property: property)
    }
    
    public init(property: String? = nil) {
        self.property = property
    }
    
    private func resolve(property: String) -> T! {
        return App.shared.assembler.resolver.property(property)
    }
    
    private func resolve() -> T! {
        let resolver = (App.shared.assembler.resolver as! Container).synchronize()
        return resolver.resolve(T.self)!
    }
}
