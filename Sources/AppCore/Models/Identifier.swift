import Foundation

public typealias Identifier = String

public protocol IdentifierType: Equatable {
    var id: Identifier { get }
}

public func ==<T: IdentifierType>(lhs: T, rhs: T) -> Bool {
    return lhs.id == rhs.id
}

public extension Identifier {
    static var empty = String()
}

public extension Array where Element: IdentifierType {
    
    @inlinable func except(elements: [Element]) -> [Element] {
        let excludedElementIds = elements.map { $0.id }
        return self.filter({ excludedElementIds.notContains($0.id) })
    }
}
