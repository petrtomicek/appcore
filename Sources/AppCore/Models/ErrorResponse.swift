import Foundation

public struct ErrorResponse: Decodable {

    public var status: String
    public var message: String?
    public var code: Int = 0

    private enum CodingKeys: String, CodingKey {
        case status, message, errors, error
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        do {
            if let status = try? container.decodeIfPresent(String.self, forKey: .status) {
                self.status = status
            } else if let status = try? container.decodeIfPresent(Int.self, forKey: .status) {
                self.status = status.description
            } else {
                status = "400"
            }
            if let message = try? container.decodeIfPresent(String.self, forKey: .message) {
                self.message = message
            }
            if let message = try? container.decodeIfPresent(ErrorMessage.self, forKey: .message) {
                self.message = message.message
                let code = message.code?.replacingOccurrences(of: "LEEAF-ERROR-", with: "")
                self.code = Int(code)
            }
            if let message = try? container.decodeIfPresent(String.self, forKey: .error) {
                self.message = message
            }
            if let errors = try? container.decodeIfPresent([ErrorMessage].self, forKey: .errors) {
                self.message = errors.first?.message
                let code = errors.first?.code?.replacingOccurrences(of: "LEEAF-ERROR-", with: "")
                self.code = Int(code)
            }
        }
    }
}

public struct ErrorMessage: Decodable {
    public var code: String?
    public var message: String?

    public init(code: String?, message: String?) {
        self.code = code
        self.message = message
    }
}
